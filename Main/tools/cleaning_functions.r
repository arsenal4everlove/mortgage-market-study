# count how many missing data in a certain variable
count_missing = function(data){
        return(sum(data %in% c(NA,'NA','')))
}

# count the number of unique values in a variable 
unique_values = function(data){
        return(length(unique(data)))
}

# find the most frequent used name
find_most = function(x){
        return(names(summary(as.factor(GroupTogether[GroupTogether$id == x,1])))[1])
}

# matching banks' name 
name_match = function(keyword,target_dataset){
        pattern = paste0('(?i)(?=.*\\b',keyword,'\\b)')
        return(subset(unique_Name, grepl(pattern, target_dataset,perl = TRUE)))
}


# multiple patial replacement 
m_replace = function(patterns,old_data,new_pattern){
        for( i in patterns){
                pattern_type = unlist(strsplit(i,''))
                if (last(pattern_type) == ']' & pattern_type[1] == '['){
                        old_data = lapply(old_data,function(x) gsub(i,new_pattern[3],x,perl = T))
                }else if(last(pattern_type) == ']'){
                        old_data = lapply(old_data,function(x) gsub(i,new_pattern[2],x,perl = T))
                }else{
                        old_data = lapply(old_data,function(x) gsub(i,new_pattern[1],x,perl = T))
                }
        }
        return(old_data)
}

# multiple replacement 
m_replace_all = function(patterns,old_data,new_pattern){
        for(i in 1:length(patterns)){
                old_data = lapply(old_data,function(x) if (x == patterns[i]) {new_pattern[i]} else {x})
        }
        return(old_data)
}

# voting method to detect loan outlier for observations with income
nonmissing_outlier = function(loan_amount,mad,median,mean,sd,ratio){ 
        if (!is.na(ratio)){
                return(sum(((abs(loan_amount - median)/mad) > 3.5),((abs(loan_amount-mean)/sd) > 3),(ratio>6.4167)))
        } 
} 

# voting method to detect loan outlier for observations without income
missing_outlier = function(loan_amount,mad,median,mean,sd,quantile){ 
        return(sum(((abs(loan_amount - median)/mad) > 2),((abs(loan_amount-mean)/sd) > 2),(loan_amount>quantile)))
}

