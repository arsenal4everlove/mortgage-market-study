# It is a function to Merge the application data with institution 
# data such that each loan application is assigned a ???Respondent_Name???
# a new attribute Loan_Group buckets ???Loan_Amount_000??? into reasonable groups

hmda_init = function(){
        # load packages
        library("dplyr")
        library("data.table")
        # read in application data and institution data
        application_data = fread('/Users/Arsenal4ever/Desktop/data-challenge-data-master/Main/data/2012_to_2014_loans_data.csv',
                                colClasses = list(integer = c(2,3,7,11),numeric = 21,character = c(1,4:6,8:10,12:20,22:24)),
                                na.strings=c("NA","NA  "))
        institution_data = fread('/Users/Arsenal4ever/Desktop/data-challenge-data-master/Main/data/2012_to_2014_institutions_data.csv',
                                 colClasses = c(Agency_Code = 'character'))
        #return left join data
        return(left_join(application_data,institution_data,by = c('Agency_Code' = 'Agency_Code','Respondent_ID' = 'Respondent_ID','As_of_Year' = 'As_of_Year')))
        
}


hmda_to_json = function(data, states = 'DC', conventional_conforming = 'Y'){
        return(data%>%filter(State %in% states,Conventional_Conforming_Flag == conventional_conforming))
}


