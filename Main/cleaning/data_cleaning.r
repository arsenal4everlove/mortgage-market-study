# Import libraries and functions 
library(scales)
library(knitr)
library(dplyr)
setwd('/Users/Arsenal4ever/Desktop/data-challenge-data-master/Main/')
source('tools/hmda_init.r')
source('tools/cleaning_functions.r')

# Read Data
loan_data = hmda_init()

#############################################################################################################
# Clean Respondent Name TS variable 
#######
#Step1#
#######
## group by identifier and look into the names
GroupTogether = as.data.frame(loan_data%>%
                                      mutate(id = group_indices_(., .dots = c("Agency_Code","Respondent_ID")))%>%
                                      select(Respondent_Name_TS,id))
clean_names = lapply(1:1391, function(x) find_most(x))
all_raw_names = data.frame(before_name = unlist(raw_names),after_name = unlist(clean_names))

## to see more patterns, split unique names and create frequency table 
uniquenames = as.character(unlist(unique(loan_data$Respondent_Name_TS)))
freq_table = data.frame(words = unlist(strsplit(uniquenames,' ')))
freq_table = freq_table%>%group_by(words)%>%summarise(number = n())%>%mutate(percent = number/sum(number))%>%
        arrange(desc(number))

## get rid of all ccommas first
loan_data$Respondent_Name_TS = gsub(',','',loan_data$Respondent_Name_TS)
## clean suffixes
unique_Name = data.frame(names = unique(loan_data$Respondent_Name_TS))
## clean NAs
loan_data$Respondent_Name_TS = m_replace(c('[ ]NA$','[ ]N\\.A\\.$','[ ]NA[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' NATIONAL ASSOCIATION','NATIONAL ASSOCIATION ',' NATIONAL ASSOCIATION '))
## clean INCs
loan_data$Respondent_Name_TS = m_replace(c('[ ]INC$','[ ]INC[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' INC.','INC. ',' INC. '))
## clean CUs
loan_data$Respondent_Name_TS = m_replace(c('[ ]C\\.U\\.$','^C\\.U\\.[ ]','[ ]CU[ ]','[ ]CU$','^CU[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' CREDIT UNION','CREDIT UNION ',' CREDIT UNION '))
## clean FCUs
loan_data$Respondent_Name_TS = m_replace(c('[ ]FCU$','[ ]FCU[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' FEDERAL CREDIT UNION','FEDERAL CREDIT UNION ',' FEDERAL CREDIT UNION '))
## clean COs
loan_data$Respondent_Name_TS = m_replace(c('[ ]CO[ ]','[ ]CO$','[ ]CO\\.[ ]','[ ]CO\\.$','[ ]COMP$','[ ]COMPA$'),as.matrix(loan_data$Respondent_Name_TS),c(' COMPANY','COMPANY ',' COMPANY '))
## clean CORPs
loan_data$Respondent_Name_TS = m_replace(c('[ ]CORPORA$','[ ]CORPORATION$','[ ]CORP.$','[ ]CORPORATIO$','[ ]CORPORA$'),as.matrix(loan_data$Respondent_Name_TS),c(' CORP','CORP ',' CORP '))
## clean FSBs
loan_data$Respondent_Name_TS = m_replace(c('[ ]F\\.S\\.B\\.'),as.matrix(loan_data$Respondent_Name_TS),c(' FSB','FSB ',' FSB '))
## clean LTDs
loan_data$Respondent_Name_TS = m_replace(c('[ ]LTD$'),as.matrix(loan_data$Respondent_Name_TS),c(' LTD.','LTD. ',' LTD. '))
## clean LLCs
loan_data$Respondent_Name_TS = m_replace(c('[ ]LL$','[ ]L\\.L\\.C\\.$'),as.matrix(loan_data$Respondent_Name_TS),c(' LLC','LLC ',' LLC '))

## check edge cases
## replace AND by &
loan_data$Respondent_Name_TS = m_replace(c('[ ]AND[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' &','& ',' & '))
## delete empty space
loan_data$Respondent_Name_TS = m_replace(c('[ ][ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' ','',''))
loan_data$Respondent_Name_TS = m_replace(c('[ ][ ][ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' ','',''))
## cleaning Fs
loan_data$Respondent_Name_TS = m_replace(c('[ ]F[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' F','F ',' F '))
loan_data$Respondent_Name_TS = m_replace(c('[ ]F\\.[ ]C\\.[ ]U\\.$','[ ]F\\.C\\.U\\.$'),as.matrix(loan_data$Respondent_Name_TS),c(' FEDERAL CREDIT UNION','FEDERAL CREDIT UNION ',' FEDERAL CREDIT UNION '))
## clean SVG
loan_data$Respondent_Name_TS = m_replace(c('[ ]SVG[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' SAVINGS','SAVINGS ',' SAVINGS '))
## clean Bs
loan_data$Respondent_Name_TS = m_replace(c('[ ]B$'),as.matrix(loan_data$Respondent_Name_TS),c(' BANK','BANK ',' BANK '))
## clean Companys
loan_data$Respondent_Name_TS = m_replace(c('[ ]COM$', '[ ]COMPAN$'),as.matrix(loan_data$Respondent_Name_TS),c(' COMPANY','COMPANY ',' COMPANY '))
## clean Mortgages
loan_data$Respondent_Name_TS = m_replace(c('[ ]MORT[ ]','[ ]MORTAGE[ ]','[ ]MORTGA$'),as.matrix(loan_data$Respondent_Name_TS),c(' MORTGAGE','MORTGAGE ',' MORTGAGE '))
## clean Community 
loan_data$Respondent_Name_TS = m_replace(c('[ ]CMTY[ ]','^CMNTY[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' COMMUNITY','COMMUNITY ',' COMMUNITY '))
## clean LLCs
loan_data$Respondent_Name_TS = m_replace(c('[ ]LLC\\.$'),as.matrix(loan_data$Respondent_Name_TS),c(' LLC','LLC ',' LLC '))
## clean &TR
loan_data$Respondent_Name_TS = m_replace(c('[ ]&TR[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' & TRUST','& TRUST ',' & TRUST '))
## clean B&T
loan_data$Respondent_Name_TS = m_replace(c('[ ]B&T[ ]'),as.matrix(loan_data$Respondent_Name_TS),c(' BANK & TRUST','BANK & TRUST ',' BANK & TRUST '))
## After deleting all commas, unique names decrease from 1602 to 1495.

#######
#Step2#
#######
## get unique names
loan_data =  as.data.frame(loan_data)
loan_data$Respondent_Name_TS = as.character(loan_data$Respondent_Name_TS)
bank_count=loan_data%>%group_by(Respondent_Name_TS)%>%summarise(number = n())
Unique_names = bank_count$Respondent_Name_TS

## use Levenshtein distance to cluster unique names
LD  <- adist(Unique_names)
rownames(LD) <- 1:length(Unique_names)
hc <- hclust(as.dist(LD))
rect.hclust(hc,k=500)
LD_cluster = data.frame(name = bank_count$Respondent_Name_TS,number = bank_count$number,group=cutree(hc,k=500))

## clean more edge cases found by looking into the clusters
patterns = c('BRIDGEVIEW BANK MORTGAGE COMPANY','BURKE & HERBERT BANK & TRUST',
             'CALHOUN COUNTY BANK','CARDINAL FINANCIAL COMPANY LP',
             'CASHCALL','COLONIAL VIRGNIA BANK',
             'FREDERICK COUNTY BANK','SUNTRUST BANKS INC.',
             'OAK MORTGAGE COMPANYLLC','CROSSCOUNTRY MORTGAGE',
             'DOOLIN SECURITY SAVINGS BANK F','E MORTGAGE MANAMGEMENT LLC',
             'EDUCATIONAL SYSTEMS F CREDIT UNION','FAIRWAY INDEPENDENT MORT. CORP',
             'THE FIDELITY BANK','FIRST & CITIZENS BANK',
             'FIRSTSHORE FEDERAL SAVINGS & LOAN ASSOCIATION','FIRST PRIORITY FEDERAL CREDIT',
             'FIRST PRIORITY FINANCIAL','FRANLKIN AMERICAN MORTGAGE COMPANY',
             'GLEN BURNIE MUTUAL SAVINGS BNK','TOTAL MORTGAGE SERVICE LLC',
             'GUARANTY BANK FSB','HAMILTON GROUP FUNDING',
             'IBM SOUTHEAST EMP CREDIT UNION','KENTUCKY FARMERS BANK CORP',
             'LEADERONE FINANCIAL CORP ISAOA','LIBERTY BANK OF MD',
             'MID AMERICA MORTGAGE','MORGAN STANLEY PRIVATE BANK N',
             'MORTGAGE MASTER','MUNICIPAL EMPLOYEES CR UNION',
             'PEOPLES BANK NATIONAL ASSOCIA','RESIDENTIAL HOME FUNDING',
             'SPERRY ASSOCIATIES FEDERAL CREDIT UNION','F&M BANK',
             'TIDEWATER MORTGAGE SERVICES','TOWN SQUARE BANK',
             'UNION FIRST MARKET BANK','UNITED SOUTHWEST MORTGAGE CORP',
             'VIRGINIA CREDIT UNION','WYNDHAM CAPITAL MORTGAGE',
             'BULLLDOG FEDERA CREDIT UNION','TRULIANT FEDERAL CREDITUNION',
             '1ST ADVANTAGE FEDERAL CREDIT','1ST COLONIAL COMMUNITYBANK',
             'BLUERIDGE BANK','1ST PREFERENCE MORTGAGE',
             'ABERDEEDN PROVING GROUND FEDERAL CREDIT UNION','MIDLAND MORTGAGE CORP',
             'VAN DYK MORTGAGE CORP','CF BANK',
             'CNB','ALCOVA MORTGAGE',
             'LSI MORTGAGE-PLUS','AMERICAN BANK & TRUST CREDIT NATIONAL ASSOCIATI',
             'IAB FINACIAL BANK','M/I FINANCIAL CORP',
             'APPALACHAIN COMMUNITY FEDERAL CREDIT UNION','LAND/HOME FINANCIAL SERVICES',
             'ASSOCIATED MORTGAGE BANKERS I','ATLANTIC BAY MORTGAGE GROUPLLC',
             'SIGNAL FINANCIAL FEDERAL CRED UNION','BALTIMORE COUNTY EMPOYEES FEDERAL CREDIT UNION',
             'BANK FUND STAFF FEDERAL CREDIT UNION','THE BANK OF DELMARVA',
             'BARRINGTON BANK & TRUST','DAMASCUS COMMUNITYBANK')
new_patterns = c('BRIDGEVIEW BANK MORTGAGE COMPANY LLC','BURKE & HERBERT BANK & TRUST COMPANY',
                 'CALHOUN COUNTY BANK INC.','CARDINAL FINANCIAL COMPANY',
                 'CASHCALL INC.','COLONIAL VIRGINIA BANK',
                 'FREDERICK COUNTY BANK','SUNTRUST BANK INC.',
                 'OAK MORTGAGE COMPANY LLC','CROSSCOUNTRY MORTGAGE INC.',
                 'DOOLIN SECURITY SAVINGS BANK','E MORTGAGE MANAGEMENT LLC',
                 'EDUCATIONAL SYSTEMS FEDERAL CREDIT UNION','FAIRWAY INDEPENDENT MORTGAGE CORP',
                 'FIDELITY BANK','FIRST CITIZENS BANK',
                 'FIRST SHORE FEDERAL SAVINGS & LOAN ASSOCIATION','FIRST PRIORITY FEDERAL CREDIT UNION',
                 'FIRST PRIORITY FINANCIAL INC.','FRANKLIN AMERICAN MORTGAGE COMPANY',
                 'GLEN BURNIE MUTUAL SAVNGS BANK','TOTAL MORTGAGE SERVICES LLC',
                 'GUARANTY BANK','HAMILTON GROUP FUNDING INC.',
                 'IBM SOUTHEAST EMPLOYEES FEDERAL CREDIT UNION','KENTUCKY FARMERS BANK',
                 'LEADERONE FINANCIAL CORP','LIBERTY BANK OF MARYLAND',
                 'MID AMERICA MORTGAGE INC.','MORGAN STANLEY PRIVATE BANK NATIONAL ASSOCIATION',
                 'MORTGAGE MASTER INC.','MUNICIPAL EMPLOYEES CREDIT UNION',
                 'PEOPLES NATIONAL BANK NATIONAL ASSOCIATION','RESIDENTIAL HOME FUNDING CORP',
                 'SPERRY ASSOCIATES FEDERAL CREDIT UNION','THE FARMERS & MECHANICS BANK',
                 'TIDEWATER MORTGAGE SERVICES INC.','TOWN SQUARE BANK INC.',
                 'UNION FIRST MARKET BANKSHARES','UNITED SOUTHWEST MORTGAGE CORP INC.',
                 'VIRGINIA CREDIT UNION INC.','WYNDHAM CAPITAL MORTGAGE INC.',
                 'BULLDOG FEDERAL CREDIT UNION','TRULIANT FEDERAL CREDIT UNION',
                 '1ST ADVANTAGE FEDERAL CREDIT UNION','1ST COLONIAL COMMUNITY BANK',
                 'BLUE RIDGE BANK','1ST PREFERENCE MORTGAGE CORP',
                 'ABERDEEN PROVING GROUND FEDERAL CREDIT UNION','MID-ISLAND MORTGAGE CORP',
                 'VANDYK MORTGAGE CORP','CFBANK',
                 'CNB BANK','ALCOVA MORTGAGE LLC',
                 'LSI MORTGAGE PLUS','AMERICAN BANK & TRUST COMPANY NATIONAL ASSOCIATION',
                 'IAB FINANCIAL BANK','M/I FINANCIAL LLC',
                 'APPALACHIAN COMMUNITY FEDERAL CREDIT UNION','LAND HOME FINANCIAL SERVICES',
                 'ASSOCIATED MORTGAGE BANKERS IN','ATLANTIC BAY MORTGAGE GRP LLC',
                 'SIGNAL FINANCIAL FEDERAL CREDIT UNION','BALTIMORE COUNTY EMPLOYEES FEDERAL CREDIT UNION',
                 'BANK-FUND STAFF FEDERAL CREDIT UNION','BANK OF DELMARVA',
                 'BARRINGTON BANK & TRUST CO.NA','DAMASCUS COMMUNITY BANK')

loan_data$Respondent_Name_TS = m_replace_all(patterns,as.matrix(loan_data$Respondent_Name_TS),new_patterns)

## more edge cases
loan_data$Respondent_Name_TS = m_replace_all(c('DB PRIVATE WEALTH MORTGAGE','DB PRIVATE WEATH MORTGAGE LTD.'),as.matrix(loan_data$Respondent_Name_TS),rep('DB PRIVATE WEALTH MORTGAGE LTD.',2))
loan_data$Respondent_Name_TS = m_replace_all(c("STATE EMPLOYEE'S CREDIT UNION","STATE EMPLOYEES CREDIT UNION"),as.matrix(loan_data$Respondent_Name_TS),rep("STATE EMPLOYEES' CREDIT UNION",2))
loan_data$Respondent_Name_TS = m_replace_all(c('INTEGRITY FIRST FINANCIAL GROU','INTGRITY FIRST FINANCIAL GROUP'),as.matrix(loan_data$Respondent_Name_TS),rep('INTEGRITY FIRST FINANCIAL GROUP',2))
loan_data$Respondent_Name_TS = m_replace_all(c('MAIN STREET BANK','MAINSTREET BANK'),as.matrix(loan_data$Respondent_Name_TS),rep('MAIN STREET BANK CORP',2))
loan_data$Respondent_Name_TS = m_replace_all(c('SUSSEX COUNTY FEDERAL CR. UN.','SUSSEX COUNTY FEDERAL CRED UNION'),as.matrix(loan_data$Respondent_Name_TS),rep('SUSSEX COUNTY FEDERAL CREDIT UNION',2))

## after cleaning the edge cases found in the clustering, unique names decrease to 1420 and it is very close to the target 1391.

#######
#Step3#
#######
## group cleaned respondent names based on identifier and choose the name used most frequently
GroupTogether = as.data.frame(loan_data%>%mutate(id = group_indices_(., .dots = c("Agency_Code","Respondent_ID")))%>%select(Respondent_Name_TS,id))
clean_names = lapply(1:1391, function(x) find_most(x))
all_clean_names = data.frame(before_name = unlist(clean_names),id = 1:1391)
loan_data = merge(loan_data,all_clean_names,by='id',all.x = TRUE)
loan_data = loan_data[,!(names(loan_data) %in% c('id','Respondent_Name_TS'))]
colnames(loan_data)[33] = 'Respondent_Name_TS'
###########################################################################################################


# Clean Loan amount variable
#######
#Step1#
#######
## add new varibles
## add loan/income ratio
loan_data$loan_income_ratio = round(loan_data$Loan_Amount_000/loan_data$Applicant_Income_000,4)
loan_data$loan_income_ratio[which(loan_data$loan_income_ratio == Inf)] = 0
## add (loan/income ratio)/(tract to MSA/MD percentage) as debt_to_area 
loan_data$tract_MSA = as.numeric(loan_data$Tract_to_MSA_MD_Income_Pct)/100
## add debt_to_area ratio
loan_data$debt_to_area = round(loan_data$loan_income_ratio/loan_data$tract_MSA,4)
## add id for each application
loan_data = as.data.frame(loan_data%>%mutate(application_id = group_indices_(., .dots = c("As_of_Year","Agency_Code","Respondent_ID","Sequence_Number"))))
## add id for each lender
loan_data = as.data.frame(loan_data%>%mutate(lender_id = group_indices_(., .dots = c("Agency_Code","Respondent_ID"))))

## Take a look at the loan type and loan purpose
loan_data%>%group_by(Loan_Type_Description)%>%
        summarise(number = n(),min = min(Loan_Amount_000),first_quantile = quantile(Loan_Amount_000,0.25),median = median(Loan_Amount_000),mean = mean(Loan_Amount_000,na.rm = T),third_quantile = quantile(Loan_Amount_000,0.75),max = max(Loan_Amount_000))
loan_data%>%group_by(Loan_Purpose_Description)%>%
        summarise(number = n(),min = min(Loan_Amount_000),first_quantile = quantile(Loan_Amount_000,0.25),median = median(Loan_Amount_000),mean = mean(Loan_Amount_000,na.rm = T),third_quantile = quantile(Loan_Amount_000,0.75),max = max(Loan_Amount_000))

## group loan amount by Census_Tract_Number,Loan_Purpose_Description and Loan_Type_Description and then 
## calculate the mean,standard deviation,median,median absolute deviation and 95% quantile for each group
loan_data = loan_data%>%group_by(Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description)%>%
        do(mutate(., median = median(Loan_Amount_000, na.rm = TRUE)))%>%
        mutate(mad = mad(Loan_Amount_000),mean = mean(Loan_Amount_000,na.rm = T),sd=sd(Loan_Amount_000,na.rm = T),
               quantile = quantile(Loan_Amount_000,0.95,na.rm = T))


#######
#Step2#
#######
## check loan amount quality for observations with income
ratio_than_25 = loan_data%>%filter(loan_income_ratio > 25)
summary(ratio_than_25$Applicant_Income_000)
summary(loan_data$Applicant_Income_000)
outlier_application_level1 = loan_data%>%filter(loan_income_ratio > 25)%>%select(application_id)
## I consider loan amount with loan/income ratio higher than 25 as level 1 outlier
## Since it means if they use more than 50% of their income to pay for a 50 years mortgage (longest mortgage in US)
## This is highly impossible and unreasonable for banks to lend the money. 

plot(density(as.numeric((loan_data%>%filter(loan_income_ratio < 6.4167)%>%select(loan_income_ratio))[,1]),na.rm = T),main = 'Ratio')
## filter the loan amount by 99 quantile, the rest looks close to normal (a little right skewed)

## for the rest data, I use a voting method to detect outliers. There are three voters: median absolute deviation based voter, 
## standard deviation based voter and 99% quantile based voter. If all three vote for it as outlier then I think it as 
## level 2 outlier.
another_outlier = loan_data%>%filter(loan_income_ratio<=25,!is.na(Applicant_Income_000),!(Loan_Amount_000 == 0),!(sd == 0))
another_outlier$outlier = apply(another_outlier[,c('Loan_Amount_000','mad','median','mean','sd','loan_income_ratio')], 1, function(x) nonmissing_outlier(x[1],x[2],x[3],x[4],x[5],x[6]))
outlier_application_level2_1  = another_outlier%>%filter(outlier == 3)%>%select(application_id)

# take a look at the na value. they are all beacuse of the mad = 0.
View(another_outlier%>%filter(is.na(outlier))%>%select(application_id))


#######
#Step3#
#######
## For those with missing income information
missing_income = loan_data%>%filter(is.na(Applicant_Income_000),!(Loan_Amount_000 == 0))
## take a look at the loan type and loan purpose
missing_income%>%group_by(Loan_Type_Description)%>%summarise(number = n(),min = min(Loan_Amount_000),first_quantile = quantile(Loan_Amount_000,0.25),median = median(Loan_Amount_000),mean = mean(Loan_Amount_000,na.rm = T),third_quantile = quantile(Loan_Amount_000,0.75),max = max(Loan_Amount_000))
missing_income%>%group_by(Loan_Purpose_Description)%>%summarise(number = n(),min = min(Loan_Amount_000),first_quantile = quantile(Loan_Amount_000,0.25),median = median(Loan_Amount_000),mean = mean(Loan_Amount_000,na.rm = T),third_quantile = quantile(Loan_Amount_000,0.75),max = max(Loan_Amount_000))

## I still use three voter method but will be more aggressive for observations without income info
## look into the function for detail
missing_income$outlier = apply(missing_income[,c('Loan_Amount_000','mad','median','mean','sd','quantile')], 1, function(x) missing_outlier(x[1],x[2],x[3],x[4],x[5],x[6]))
outlier_application_level2_2 = missing_income%>%filter(outlier == 3)%>%select(application_id)

# take a look at na outlier
View(missing_income%>%filter(is.na(outlier))%>%select(Loan_Amount_000,median,mad,mean,sd,quantile,application_id))

# They are NA because they are the only loan provided in this area by this type and purpose
# It is also suspicious, so I will label it as level three outlier
outlier_application_level3 = missing_income%>%filter(is.na(outlier))%>%select(application_id)

# add outlier flag for loan amount
loan_data$outlier = 0
loan_data[which(loan_data$application_id %in% outlier_application_level1$application_id),44] = 1
loan_data[which(loan_data$application_id %in% outlier_application_level2_1$application_id),44] = 2
loan_data[which(loan_data$application_id %in% outlier_application_level2_2$application_id),44] = 2
loan_data[which(loan_data$application_id %in% outlier_application_level3$application_id),44] = 3

save(loan_data,file = 'data/all_cleaned.RData')