$ Agency_Code                   : chr  The agency to which the Respondent_ID "responded" with its information
 $ Respondent_ID                 : chr  When paired with the Agency_Code, provides a unique identifier for the institution that year.
 $ As_of_Year                    : int  Year in which loan was originated and for which the data was reported
 $ Applicant_Income_000          : int  The total income used for decisioning the loan in thousnads
 $ Census_Tract_Number           : chr  Identifier for the census tract in which the home was located
 $ County_Code                   : chr  Numeric code designating a county within a state in which the home was located
 $ FFIEC_Median_Family_Income    : chr  Median family income for the MSA/MD in which the census tract is located
 $ Loan_Amount_000               : int  Amount of the loan, in $000's
 $ MSA_MD                        : chr  code designating a Metropolitan Statistical Area (MSA) / Metropolitan Division (MD) in which the home was located. These codes are periodically updated, and were last updated for HMDA between the 2013 and 2014 reporting years.
 $ Number_of_Owner_Occupied_Units: chr  Number of units, in the census tract and including condominiums, in which the owner lives
 $ Sequence_Number               : int  Provides a unique identifier corresponding to an application or loan for a given responding insitution. I.e., each application at a responding institution must be assigned its own unique sequence number in a given year.
 $ State_Code                    : chr  Two-digit FIPS code for the state in which the home was located.
 $ Tract_to_MSA_MD_Income_Pct    : chr  Percentage of the MSA/MD median Family income for the census tract; 2 decimal places of precsion
 $ MSA_MD_Description            : chr  a human-readable format for describing MSA's. These map 1:1 with MSA_MD codes
 $ Loan_Purpose_Description      : chr  The purpose of the loan is whether it was used for purchasing a home, refinancing an alread-existing loan, or used for "home improvement" purposes. ("Home Improvement" loans have been removed from this data file.)
 $ Agency_Code_Description       : chr  Expanded form of the agency code for readability.
 $ Lien_Status_Description       : chr  Lien position of the loan. If first lien position, then this loan carries the least risk if the borrower defaults because the lender has first claims to proceeds from the sale of a some. Subordinate liens carry more risk because they are paid after the lender for the first lien is paid.
 $ Loan_Type_Description         : chr  Specifies whether the loan was "conventional," i.e., not backed by a government-sponsored program or government-insured or government-guaranteed
 $ State                         : chr  Standard 2-letter postal code for the state based on the State_Code given
 $ County_Name                   : chr  Name of the county as pulled in from the FHFA Conforming Loan Limits as calculated under HERA. Please note that county names are only unique within a state
 $ Conforming_Limit_000          : num  Single Unit Conforming Loan limit for the given county.
 $ Conventional_Status           : chr  Signifies whether a loan type is '1' or not.
 $ Conforming_Status             : chr  'Conforming' for loans with amounts less than their county's respective conforming loan limit. "Jumbo" if the loan amount exceeds the county limit
 $ Conventional_Conforming_Flag  : chr  'Y' if Conventional_Status is "Conventional" and "Conforming_Status" is "Conforming"
 $ Respondent_City_TS            : chr  City where the originating institution is located.
 $ Respondent_State_TS           : chr  State where the originating institution is located
 $ Respondent_ZIP_Code           : chr  State where the originating institution is located
 $ Parent_Name_TS                : chr  The reported Parent to the responding insitutuion. Parent_Name_TS may refer to another entry within the institution file. Parent_Name_TS may be blank if the responding instution has no parent to report (and hence could be regarded as its own parent in certain circumstances). Finally, a Parent institution may have multiple children institutions within the file. Parent institutions may change with each reporting year.INC." ...
 $ Parent_City_TS                : chr  City where the parent institution is located
 $ Parent_State_TS               : chr  State where the parent institution is located
 $ Parent_ZIP_Code               : chr  ZIP where the parent institution is located
 $ Assets_000_Panel              : int  Total assets reported to the "Agency" for the top parent by the top level parent corporation across the nation.
 $ Respondent_Name_TS            : name of the company providing the HMDA data (and who originated the loan)
 $ loan_income_ratio             : num  loan_amount/applicant's income 
 $ tract_MSA                     : num  convert Tract_to_MSA_MD_Income_Pct into numeric 
 $ debt_to_area                  : num  loan_income_ratio/tract_MSA
 $ application_id                : int  id for each application
 $ lender_id                     : int  id for unique lender
 $ median                        : num  median for each group, group by Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description; 
 $ mad                           : num  mad for each group , Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description
 $ mean                          : num  mean for each group, Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description
 $ sd                            : num  sd for each group, Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description
 $ quantile                      : num  0.95 quantile for each group,Census_Tract_Number,Loan_Purpose_Description,Loan_Type_Description 
 $ outlier                       : num  0 means not outlier, 1 very likely to be outlier, 2 likely to be outlier, 3 could be outlier