Zip file contains four folders:

tools folder contains the two function task 1 requires. The original data has been deleted 
to make the zip file smaller. If you want to use the function please put the original data back to the data folder.
data folder contains the cleaned metadata file and two data used to run my app
cleaning folder contains the quality checking report, data cleaning report and their code required in task 2
myApp folder contains my code for the Rshiny application. 
