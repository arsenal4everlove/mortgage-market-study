library(shiny)
library(shinydashboard)
library(reshape2)
library(ggplot2)
library(leaflet)
library(dplyr)
library(raster)
library(shiny)
library(leaflet)
library(RColorBrewer)
library(plotly)
setwd('/Users/Arsenal4ever/Desktop/data-challenge-data-master/Main/')
load('data/market_size.RData')
load('data/Risk.RData')
setwd('/Users/Arsenal4ever/Desktop/data-challenge-data-master/Main/myApp')
############################################################################################################

state_names = c("District of Columbia","Delaware","Maryland","Virginia","West Virginia")
years = c('2012','2013','2014')
metrics = c('Total_Loan_amount','Total_application','Total_unit','Unique_lenders','BigPlayers','Median_income','Application_unit_ratio')

# map data 
states = subset(getData("GADM", country = "USA", level = 1),
                NAME_1 %in% state_names)

# define color palettes for states
pal <- brewer.pal(8, "Dark2")
statePal <- colorFactor(pal, states@data$NAME_1)

############################################################################################################

header <- dashboardHeader(title = "Home Loan Market Analysis" )
sidebar <- dashboardSidebar(
        # sidebarMenuOutput("sidebarMenu") # Used for dynamic menus
        sidebarMenu(
                id = "selectedTab",
                menuItem("Market Size", tabName = "map", icon = icon("map"), selected = FALSE),
                menuItem("Risk Evaluation", tabName = "myPlot", icon = icon("bar-chart-o"))
        )
)

body <- dashboardBody(
        tabItems(
                # Map
                tabItem(tabName = "map" 
                         ,fluidRow(
                                 column(width = 9,
                                        box(
                                                title = "Home Loan Map", 
                                                status = "primary", 
                                                width = NULL,
                                                solidHeader = FALSE,
                                                collapsible = TRUE,
                                                strong(h4(textOutput(outputId = "choro.cat"))),
                                                h5(textOutput(outputId = "choro.ind")),
                                                br(),
                                                leafletOutput(outputId = "LoanMap", height = 355)
                                                
                                        )),
                                column(width = 3,
                                       box(width = NULL, status = "warning",
                                           selectInput("year", "Select Year",
                                                       choices = years,
                                                       selected = "2012"
                                           ),
                                           height = 105
                                       ),
                                       box(width = NULL, status = "warning",
                                           selectInput("metrics", "Select Metric",
                                                       choices = metrics,
                                                       selected = "Total_unit"
                                           ),
                                           p(class = "text-muted",
                                             paste("Note: Total loan amount is in thousand;",
                                                   "Only unique values are sumed when calcultate Tatotal unit;",
                                                   "BigPlayers are lenders whose asset is larger than 80% lenders."
                                                   )
                                             ),
                                           height = 322
                                       )
                                          
                                )
                                        
                        ),
                        fluidRow(
                                box(
                                     title = 'Bar Plot', 
                                     solidHeader = TRUE,
                                     collapsible = TRUE,
                                     plotOutput(outputId = "plot1")
                                   ),
                                tabBox(
                                        title = "Analysis",
                                        tabPanel("Hypothesis", "Content",verbatimTextOutput("hypo")),
                                        tabPanel("Insight", "Content",verbatimTextOutput("insight")),
                                        tabPanel("Conclusion", "Content",verbatimTextOutput("conclusion"))
                                )
                            )

                ),
                #Second tab Item
                tabItem(tabName ="myPlot" ,
                        fluidRow(
                                tabBox(width=12,height = 10,
                                       # The id lets us use input$tabset1 on the server to find the current tab
                                       id = "tabset", 
                                       
                                       tabPanel("Severe Delinquency Debt Percent in Mortgage", plotlyOutput("plot_1"),h2("General Analysis"),p("Severe delinquency is defined as having at least one account 90+ days past due. From the Plot, it is observed that the severe delinquency is going down from 2012 to 2014, which is indicating people's pressure from mortgage is releasing, at least for extreme cases.
                                                                                                         
                                                                                                         ")),
                                       tabPanel("Median Loan/Median Income Ratio", plotlyOutput("plot_2"),p("Median Loan/Median Income ratio measures the pressure from mortgageWe can see all five states' have an increasing trend in this ratio from 2012 to 2014. So even severe delinqunecny goes down, home loan market is still not in a good condition. We can see that West Virginia has the lowest ratio.
                                                                                  
                                                                                  ")),
                                       tabPanel("Debt Ratio", plotlyOutput("plot_3"),p("Debt Ratio is the most important factor during the risk evaluation for mortgage. It is shown in the graph that DC has the best debt ratio performance from 2012 to 2014, which indicates healthy mortgage market in DC and it makes entering DC's home loan market more attractive. DE has the largest debt ratio that shows larger risk in local mortgage market.  Except Maryland and DC, the rest three all show some increase in the debit ratio. So in conlusion, the home loan market is still under a bad condition. But there is still opportunity in active market such as DC."))

                                       )
                                 )
                        )
        )
)


# dashboard "user interface" is the combination of a header, sidebar and a body to form a dashboard page  
ui <- dashboardPage(header, sidebar, body, skin = "blue")
############################################################################################################

############################################################################################################
server <- function(input, output, session) {
        
        # Home loan Map 
        output$LoanMap <- renderLeaflet({
                leaflet() %>% 
                        addTiles() %>% 
                        addPolygons(data = states, 
                                    fillColor = ~statePal(states@data$NAME_1), 
                                    fillOpacity = 1, 
                                    color = "white", 
                                    stroke = T, 
                                    weight = 1,
                                    layerId = states@data$NAME_1
                                    )%>%
                        addPopups(c(-77.05017599999996,-75.18261,-77.003676,-79.121179,-81.641523),
                                  c(38.8892686,38.642491,39.582989,37.379245,38.355102),
                                  as.character(subset(market_size,As_of_Year == input$year,select = input$metrics)[[input$metrics]]),
                                  options = popupOptions(closeButton = FALSE))
        })
        
        output$plot1 <- renderPlot({
                data <- subset(market_size,select = c(input$metrics,'As_of_Year','State'))
                ggplot(data, aes_string('State',input$metrics,fill = 'As_of_Year')) + 
                        geom_bar(stat="identity", position=position_dodge())
        })
        
        output$hypo <- renderText({
                includeText("hypo.txt")
        })
        
        output$insight <- renderText({
                includeText(paste0(input$metrics,'.txt'))
        })
        
        output$conclusion <- renderText({
                includeText("Conclusion.txt")
        })
        
        output$plot_1<-renderPlotly({plot_ly(Risk,x = State, y = percent_severe_Dl_debt, color = Year,type = 'bar')})
        output$plot_2<-renderPlotly({plot_ly(Risk,x = State, y = median_loan_income_ratio, color = Year,type = 'bar')})
        output$plot_3<-renderPlotly({plot_ly(Risk,x = State, y = debt_income, color = Year,type = 'bar')})
        
        

}

shinyApp(ui, server) 