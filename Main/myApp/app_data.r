# Market size metric
loan_data$Number_of_Owner_Occupied_Units =  as.numeric(loan_data$Number_of_Owner_Occupied_Units)
state_names = c("District of Columbia","Delaware","Maryland","Virginia","West Virginia")
years = c('2012','2013','2014')
metrics = c('Total_Loan_amount','Total_application','Total_unit','Unique_lenders','BigPlayers','Median_income','Application_unit_ratio')
Big_players_rule = quantile(loan_data$Assets_000_Panel,0.8)
market_size = loan_data%>%group_by(State,As_of_Year)%>%
        summarise(Total_Loan_amount = sum(Loan_Amount_000),
                  Total_application = n(),
                  Total_unit = sum(unique(Number_of_Owner_Occupied_Units),na.rm = T),
                  Unique_lenders = length(unique(lender_id)),
                  BigPlayers = sum(Assets_000_Panel>Big_players_rule),
                  Median_income = median(Applicant_Income_000,na.rm = T)
        )%>%
        mutate(Application_unit_ratio = Total_application/Total_unit,
               BigPlayer_ratio = BigPlayers/Total_application)
market_size$As_of_Year = as.factor(market_size$As_of_Year)

# Risk metric 
Risk_1 = data.frame(State=c(rep("MD",3),rep("VA",3),rep("DC",3),rep("WV",3),rep("DE",3)),
                    Year = c(rep(c("2012","2013","2014"),5)),
                    Median_Det = c(42771,39274,39272,36284,34613,34683,26705,26025,26391,22135,
                                   22184,22564,34647,33795,35202),
                    percent_severe_Dl_debt = c(6.1,6,4.5,3.2,2.9,2.2,5.2,5,3.9,3,2.9,2.6,5.5,5.5,4.6)
)

Risk_2=loan_data%>%group_by(State,As_of_Year)%>%summarise(median_income = median(Applicant_Income_000,na.rm = T),
                                                          median_loan_income_ratio = median(loan_income_ratio,na.rm = T))
Risk_2$State = as.factor(Risk_2$State)
Risk_2$As_of_Year = as.factor(Risk_2$As_of_Year)
Risk = left_join(Risk_1,Risk_2,by = c('State' = 'State','Year' = 'As_of_Year'))
Risk$debt_income = Risk$Median_Det/(Risk$median_income*1000)

save(market_size,file = 'data/market_size.RData')
save(Risk,file = 'data/Risk.RData')